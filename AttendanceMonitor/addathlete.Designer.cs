﻿namespace AttendanceMonitor
{
    partial class addathlete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtfname = new DevExpress.XtraEditors.TextEdit();
            this.txtcourse = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnadd = new DevExpress.XtraEditors.SimpleButton();
            this.txtid = new System.Windows.Forms.TextBox();
            this.cmbday = new System.Windows.Forms.ComboBox();
            this.eventsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.txtfname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(66, 67);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(102, 21);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Student no.";
            // 
            // txtfname
            // 
            this.txtfname.Location = new System.Drawing.Point(184, 107);
            this.txtfname.Name = "txtfname";
            this.txtfname.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfname.Properties.Appearance.Options.UseFont = true;
            this.txtfname.Size = new System.Drawing.Size(276, 28);
            this.txtfname.TabIndex = 2;
            // 
            // txtcourse
            // 
            this.txtcourse.Location = new System.Drawing.Point(184, 154);
            this.txtcourse.Name = "txtcourse";
            this.txtcourse.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcourse.Properties.Appearance.Options.UseFont = true;
            this.txtcourse.Size = new System.Drawing.Size(276, 28);
            this.txtcourse.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(114, 110);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 21);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Name";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(106, 157);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 21);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Course";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(131, 204);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 21);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Day";
            // 
            // btnadd
            // 
            this.btnadd.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Appearance.Options.UseFont = true;
            this.btnadd.Location = new System.Drawing.Point(184, 249);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(276, 40);
            this.btnadd.TabIndex = 10;
            this.btnadd.Text = "Add Participant";
            this.btnadd.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // txtid
            // 
            this.txtid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.txtid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtid.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtid.Location = new System.Drawing.Point(184, 60);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(276, 30);
            this.txtid.TabIndex = 1;
            this.txtid.TextChanged += new System.EventHandler(this.txtid_TextChanged);
            // 
            // cmbday
            // 
            this.cmbday.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.eventsBindingSource, "Days", true));
            this.cmbday.FormattingEnabled = true;
            this.cmbday.Location = new System.Drawing.Point(184, 204);
            this.cmbday.Name = "cmbday";
            this.cmbday.Size = new System.Drawing.Size(121, 24);
            this.cmbday.TabIndex = 11;
            // 
            // eventsBindingSource
            // 
            this.eventsBindingSource.DataSource = typeof(AttendanceMonitor.Events);
            // 
            // addathlete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 349);
            this.Controls.Add(this.cmbday);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtcourse);
            this.Controls.Add(this.txtfname);
            this.Controls.Add(this.labelControl1);
            this.Name = "addathlete";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Participant";
            this.Load += new System.EventHandler(this.addathlete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtfname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtfname;
        private DevExpress.XtraEditors.TextEdit txtcourse;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnadd;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.ComboBox cmbday;
        private System.Windows.Forms.BindingSource eventsBindingSource;
    }
}