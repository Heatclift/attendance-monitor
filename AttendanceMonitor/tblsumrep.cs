﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttendanceMonitor
{
    class tblsumrep
    {
        public string studid { get; set; }
        public string name { get; set; }
        public string no_of_present { get; set; }
        public string no_of_absences { get; set; }
    }
}
