﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class AddStud : DevExpress.XtraEditors.XtraForm
    {
        public AddStud()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void labelControl5_Click(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void labelControl3_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            if (txtcourse.Text.Count() > 0 && txtfname.Text.Count() > 0 && txtlname.Text.Count() > 0 && txtsection.Text.Count() > 0 && txtstudid.Text.Count() > 0)
            {
                Student_Class_List stud = new Student_Class_List();
                stud.Student_No = txtstudid.Text;
                stud.Last_Name = txtlname.Text;
                stud.First_Name = txtfname.Text;
                stud.Program = txtcourse.Text;
                stud.Section = txtsection.Text;

                try
                {
                    db.Student_Class_List.Add(stud);
                    db.SaveChanges();
                    this.Dispose();
                }
                catch
                {
                    MessageBox.Show(this, "Can't connect to the Data Base", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(this, "Pleas fill out all fields", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
    }
}
