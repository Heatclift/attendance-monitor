﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;

namespace AttendanceMonitor
{
    public partial class detailedrep : DevExpress.XtraReports.UI.XtraReport
    {
        public detailedrep()
        {
            InitializeComponent();
        }
        public void InitData(List<tbldetailedrep> data, string cours,string day)
        {
           
            objectDataSource1.DataSource = data;
            Course.Value = cours;
            Day.Value = day;
            
        }
    }
}
