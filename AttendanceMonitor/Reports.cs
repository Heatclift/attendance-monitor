﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Reports : DevExpress.XtraEditors.XtraForm
    {
        public Reports()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }
        public void poplulate()
        {
            ////course
            List<string> courses = new List<string>();
            courses.Add(db.Student_Class_List.FirstOrDefault().Program);
            foreach(var i in db.Student_Class_List)
            {
              if(!courses.Contains(i.Program.Replace(" ", string.Empty)))
                {
                    courses.Add(i.Program);
                }
            }
            cmbcs.Properties.Items.Clear();
            cmbcs.Properties.Items.AddRange(courses);
            ///end of courses
            ///
            cmbevent.Properties.Items.Clear();
            List<string> lstd = new List<string>();

            for (int i = 1; i <= db.Days.Where(x => x.day == "days").FirstOrDefault().dayno; i++)
            {
                lstd.Add("Day " + Convert.ToString(i));
            }
            int day = 0;
            try
            {
                day = Convert.ToInt32(cmbevent.Text.Replace("Day ", String.Empty)); ;
            }
            catch
            {
                day = 0;
            }
           
            cmbevent.Properties.Items.AddRange(lstd);
            List<string> presentlist = db.Attendancelist.Where(y => y.Events.@event == cmbevent.Text).Select(x => x.fk_studid).ToList();
            chart1.Series["Att"].Points.Clear();
            chart1.Series["Att"].Points.Add(new DevExpress.XtraCharts.SeriesPoint("Present",db.Attendancelist.Where(x => x.state == "OUT" && x.Student_Class_List.Program == cmbcs.Text && x.day == day).Count()));//// to do here\/
            chart1.Series["Att"].Points.Add(new DevExpress.XtraCharts.SeriesPoint("Absent", db.Student_Class_List.Where(x => !presentlist.Contains(x.Student_No) && x.Program == cmbcs.Text).Count()));
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            poplulate();
        }

        private void cmbcs_SelectedIndexChanged(object sender, EventArgs e)
        {
            poplulate();
        }

        private void cmbevent_SelectedIndexChanged(object sender, EventArgs e)
        {
            poplulate();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
           
                    printfrm prtfrm = new printfrm(false, cmbevent.Text, cmbcs.Text,cmbrep.Text);
                    prtfrm.ShowDialog();
                
        }
    }
}
