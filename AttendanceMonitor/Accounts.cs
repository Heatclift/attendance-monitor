﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Accounts : DevExpress.XtraEditors.XtraForm
    {
        public Accounts()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            adduser frm = new adduser();
            frm.ShowDialog();
        }

        private void Accounts_Load(object sender, EventArgs e)
        {
            refresh();
        }
        private void refresh()
        {
            List<Useraccts> acc = db.Useraccts.ToList();
            dgtvacct.DataSource = HelpTools.ToDataTable(acc);
        }

        private void Accounts_Activated(object sender, EventArgs e)
        {
            refresh();
        }

        private void cntx1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void rEmoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow rows in dgtvacct.SelectedRows)
            {

                int id = Convert.ToInt32(rows.Cells["iDDataGridViewTextBoxColumn"].Value);
                Useraccts recrev = db.Useraccts.Where(x => x.ID == id).FirstOrDefault();
                db.Useraccts.Remove(recrev);

            }
            if (MessageBox.Show("Are you sure you want to REMOVE?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {

                db.SaveChanges();
            }
            refresh();
        }
    }
}
