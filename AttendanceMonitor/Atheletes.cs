﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Atheletes : DevExpress.XtraEditors.XtraForm
    {
        AttendanceEntities1 db = new AttendanceEntities1();
        public Atheletes()
        {
            InitializeComponent();
        }

        private void btnaddath_Click(object sender, EventArgs e)
        {
            addathlete frm = new addathlete();
            frm.ShowDialog();
        }

        private void Atheletes_Load(object sender, EventArgs e)
        {
            refresh();
        }
        private void refresh()
        {
            List<Athletes> at = new List<Athletes>();
            AttendanceEntities1 rdb = new AttendanceEntities1();
            foreach (var i in rdb.Athletes.ToList())
            {
                Athletes model = new Athletes();
                model.@event = i.@event;
                model.sudentname = i.Student_Class_List.First_Name + " " + i.Student_Class_List.Last_Name;
                model.ID = i.ID;
                model.studno = i.studno;
                model.curse = i.Student_Class_List.Program;
                at.Add(model);
            }
            dtgvparticipants.DataSource = at;
        }

        private void Atheletes_Enter(object sender, EventArgs e)
        {
            refresh();
        }

        private void Atheletes_Activated(object sender, EventArgs e)
        {
            refresh();
        }

        private void rEmoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow rows in dtgvparticipants.SelectedRows)
            {

                int id = Convert.ToInt32(rows.Cells["iDDataGridViewTextBoxColumn"].Value);
                Athletes recrev = db.Athletes.Where(x => x.ID == id).FirstOrDefault();
                db.Athletes.Remove(recrev);

            }
            if (MessageBox.Show("Are you sure you want to REMOVE?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {

                db.SaveChanges();
            }
            refresh();
        }
    }
}
