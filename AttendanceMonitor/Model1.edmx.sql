
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/18/2020 02:00:34
-- Generated from EDMX file: D:\SRCs\C#\Attendance_monitor\AttendanceMonitor\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Attendance];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Athletes_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Athletes] DROP CONSTRAINT [FK_Athletes_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_Athletes_Student_Class_List]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Athletes] DROP CONSTRAINT [FK_Athletes_Student_Class_List];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendancelist_Events]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendancelist] DROP CONSTRAINT [FK_Attendancelist_Events];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendancelist_Student_Class_List]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendancelist] DROP CONSTRAINT [FK_Attendancelist_Student_Class_List];
GO
IF OBJECT_ID(N'[dbo].[FK_Divisions_Divisions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Divisions] DROP CONSTRAINT [FK_Divisions_Divisions];
GO
IF OBJECT_ID(N'[dbo].[FK_Events_Days]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_Events_Days];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Athletes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Athletes];
GO
IF OBJECT_ID(N'[dbo].[Attendancelist]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attendancelist];
GO
IF OBJECT_ID(N'[dbo].[Days]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Days];
GO
IF OBJECT_ID(N'[dbo].[Divisions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Divisions];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[Student_Class_List]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Student_Class_List];
GO
IF OBJECT_ID(N'[dbo].[Useraccts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Useraccts];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Athletes'
CREATE TABLE [dbo].[Athletes] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [studno] varchar(50)  NULL,
    [event] varchar(50)  NULL,
    [fk_event] int  NULL
);
GO

-- Creating table 'Attendancelist'
CREATE TABLE [dbo].[Attendancelist] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [fk_studid] varchar(50)  NULL,
    [fk_event] int  NULL,
    [date_time] datetime  NULL,
    [state] varchar(10)  NULL,
    [type] varchar(50)  NULL,
    [day] int  NULL
);
GO

-- Creating table 'Days'
CREATE TABLE [dbo].[Days] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [day] varchar(50)  NULL,
    [dayno] int  NULL
);
GO

-- Creating table 'Divisions'
CREATE TABLE [dbo].[Divisions] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Division] varchar(50)  NULL,
    [fk_Course] varchar(50)  NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [event] varchar(50)  NULL,
    [date_time] datetime  NULL,
    [fk_day] int  NULL
);
GO

-- Creating table 'Student_Class_List'
CREATE TABLE [dbo].[Student_Class_List] (
    [Student_No] varchar(50)  NOT NULL,
    [Omega_ID] varchar(50)  NULL,
    [Column_2] varchar(50)  NULL,
    [Column_3] varchar(50)  NULL,
    [Campus] varchar(50)  NULL,
    [Column_5] varchar(50)  NULL,
    [Last_Name] varchar(50)  NULL,
    [Column_7] varchar(50)  NULL,
    [First_Name] varchar(50)  NULL,
    [Academic_Career] varchar(50)  NULL,
    [Program] varchar(50)  NULL,
    [Course] varchar(50)  NULL,
    [Section] varchar(50)  NULL
);
GO

-- Creating table 'Useraccts'
CREATE TABLE [dbo].[Useraccts] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [user] varchar(50)  NULL,
    [password] varchar(50)  NULL,
    [usrtype] varchar(50)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Athletes'
ALTER TABLE [dbo].[Athletes]
ADD CONSTRAINT [PK_Athletes]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Attendancelist'
ALTER TABLE [dbo].[Attendancelist]
ADD CONSTRAINT [PK_Attendancelist]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Days'
ALTER TABLE [dbo].[Days]
ADD CONSTRAINT [PK_Days]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [PK_Divisions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Student_No] in table 'Student_Class_List'
ALTER TABLE [dbo].[Student_Class_List]
ADD CONSTRAINT [PK_Student_Class_List]
    PRIMARY KEY CLUSTERED ([Student_No] ASC);
GO

-- Creating primary key on [ID] in table 'Useraccts'
ALTER TABLE [dbo].[Useraccts]
ADD CONSTRAINT [PK_Useraccts]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [fk_event] in table 'Athletes'
ALTER TABLE [dbo].[Athletes]
ADD CONSTRAINT [FK_Athletes_Events]
    FOREIGN KEY ([fk_event])
    REFERENCES [dbo].[Events]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Athletes_Events'
CREATE INDEX [IX_FK_Athletes_Events]
ON [dbo].[Athletes]
    ([fk_event]);
GO

-- Creating foreign key on [studno] in table 'Athletes'
ALTER TABLE [dbo].[Athletes]
ADD CONSTRAINT [FK_Athletes_Student_Class_List]
    FOREIGN KEY ([studno])
    REFERENCES [dbo].[Student_Class_List]
        ([Student_No])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Athletes_Student_Class_List'
CREATE INDEX [IX_FK_Athletes_Student_Class_List]
ON [dbo].[Athletes]
    ([studno]);
GO

-- Creating foreign key on [fk_event] in table 'Attendancelist'
ALTER TABLE [dbo].[Attendancelist]
ADD CONSTRAINT [FK_Attendancelist_Events]
    FOREIGN KEY ([fk_event])
    REFERENCES [dbo].[Events]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendancelist_Events'
CREATE INDEX [IX_FK_Attendancelist_Events]
ON [dbo].[Attendancelist]
    ([fk_event]);
GO

-- Creating foreign key on [fk_studid] in table 'Attendancelist'
ALTER TABLE [dbo].[Attendancelist]
ADD CONSTRAINT [FK_Attendancelist_Student_Class_List]
    FOREIGN KEY ([fk_studid])
    REFERENCES [dbo].[Student_Class_List]
        ([Student_No])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendancelist_Student_Class_List'
CREATE INDEX [IX_FK_Attendancelist_Student_Class_List]
ON [dbo].[Attendancelist]
    ([fk_studid]);
GO

-- Creating foreign key on [fk_day] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_Events_Days]
    FOREIGN KEY ([fk_day])
    REFERENCES [dbo].[Days]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Events_Days'
CREATE INDEX [IX_FK_Events_Days]
ON [dbo].[Events]
    ([fk_day]);
GO

-- Creating foreign key on [ID] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [FK_Divisions_Divisions]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[Divisions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------