﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Attendancefrm : DevExpress.XtraEditors.XtraForm
    {
        AttendanceEntities1 db = new AttendanceEntities1();
        public Attendancefrm( )
        {
            InitializeComponent();
            
        }
        Timer tmr = new Timer();
        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void labelControl5_Click(object sender, EventArgs e)
        {

        }
        private void time(object sender, EventArgs e)
        {
            //get current time

            lblhour.Text = DateTime.Now.ToString("hh:mm:ss tt");
        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        public void excepmt()
        {
            AttendanceEntities1 rdb = new AttendanceEntities1();
            AttendanceEntities1 rdb2 = new AttendanceEntities1();
            foreach (var i in rdb.Athletes)
            {
                Attendancelist model = new Attendancelist();
                int dayno =Convert.ToInt32(cmbevent.Text.Replace("Day ", string.Empty));
                if (i.@event == cmbevent.Text && rdb2.Attendancelist.Where(x => x.fk_studid == i.studno && x.day == dayno).Count() == 0)
                {
                   
                    model.fk_studid = i.studno;
                    model.fk_event = i.fk_event;
                    model.day = Convert.ToInt32(cmbevent.Text.Replace("Day ", string.Empty));
                    model.date_time = DateTime.Now;
                    model.state = "IN";
                    model.type = "AM";
                    db.Attendancelist.Add(model);
                    model.date_time = DateTime.Now.AddHours(5);
                    model.state = "OUT";
                    model.type = "AM";
                    db.Attendancelist.Add(model);
                    model.date_time = DateTime.Now.AddHours(10);
                    model.state = "IN";
                    model.type = "PM";
                    db.Attendancelist.Add(model);
                    model.date_time = DateTime.Now.AddHours(15);
                    model.state = "OUT";
                    model.type = "PM";
                    db.Attendancelist.Add(model);
                    db.SaveChanges();
                }
            }
        }
        private void Attendancefrm_Load(object sender, EventArgs e)
        {
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(this.time);
            tmr.Start();
            string[] lst = db.Student_Class_List.Select(x => x.Student_No).ToArray();
            var autosrc = new AutoCompleteStringCollection();
            autosrc.AddRange(lst);
            txtid.AutoCompleteCustomSource = autosrc;
            List < string > lstd = new List<string>();
            
                for (int i = 1; i <= db.Days.Where(x => x.day =="days").FirstOrDefault().dayno; i++)
                {
                    lstd.Add("Day " + Convert.ToString(i));
                }
           
            cmbevent.Properties.Items.AddRange(lstd.ToArray());
            excepmt();
        }

        private void txtid_TextChanged(object sender, EventArgs e)
        {
            txtname.Text = db.Student_Class_List.Where(x => x.Student_No == txtid.Text).Select( y => y.First_Name + " " +y.Last_Name).FirstOrDefault();
            txtcourse.Text = db.Student_Class_List.Where(x => x.Student_No == txtid.Text).Select(y => y.Program).FirstOrDefault();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            addattendace();
        }
        private void addattendace()
        {
            Attendancelist lis = new Attendancelist();
           
            lis.day = Convert.ToInt32(cmbevent.Text.Replace("Day ", string.Empty));
            lis.state = cmbstate.Text;
            lis.fk_studid = txtid.Text;
            lis.date_time = DateTime.Now;
            lis.type = cmbhalf.Text;
            if (cmbevent.Text != "Choose Event")
            {
                if (txtid.Text != "")
                {
                    if (MessageBox.Show(this, btnattend.Text + "?", "Attend Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (db.Attendancelist.Where(x => x.fk_studid == txtid.Text && x.day == lis.day && x.state == lis.state && x.type == lis.type).Count() == 0)
                        {
                            try
                            {
                                if(db.Student_Class_List.Where(x => x.Student_No == txtid.Text).Count() > 0)
                                {
                                    db.Attendancelist.Add(lis);
                                    db.SaveChanges();
                                    MessageBox.Show(this, "Attendance recorded!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtprevname.ForeColor = Color.Black;
                                    txtprevname.BackColor = Color.Lime;
                                    txtprevname.Text = txtid.Text + ", " + txtname.Text + ", " + txtcourse.Text + ", " + cmbstate.Text;
                                }
                                else
                                {
                                    MessageBox.Show(this, "404 Student not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    txtprevname.ForeColor = Color.Black;
                                    txtprevname.BackColor = Color.Red;
                                    txtprevname.Text = "404 Student not found!";
                                }
                               
                            }
                            catch
                            {
                                 MessageBox.Show(this, "404 Student not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                txtprevname.ForeColor = Color.Black;
                                txtprevname.BackColor = Color.Red;
                                txtprevname.Text = "404 Student not found!";
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Attendance already recorded", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtprevname.ForeColor = Color.Black;
                            txtprevname.BackColor = Color.Yellow;
                            txtprevname.Text = txtid.Text + ", " + txtname.Text + ", " + txtcourse.Text + ", " + cmbstate.Text;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(this, "Student no is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtprevname.Text = "Student no is empty";
                }
            }
            else
            {
                MessageBox.Show(this, "Pleasee Choose an Event", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtprevname.ForeColor = Color.Black;
                txtprevname.BackColor = Color.Red;
                txtprevname.Text = txtid.Text + ", " + txtname.Text + ", " + txtcourse.Text + ", " + cmbstate.Text;
            }
            
            txtid.Clear();
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void cmbstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnattend.Text = "Attendance "+ cmbstate.Text;
        }

        private void labelControl1_Click_1(object sender, EventArgs e)
        {

        }

        private void txtid_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F1)
            {
                addattendace();
            }
        }

        private void cmbevent_SelectedIndexChanged(object sender, EventArgs e)
        {
            excepmt();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Exit Application?", "Exit?",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            } 
        }
    }
}
