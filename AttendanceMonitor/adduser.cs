﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class adduser : DevExpress.XtraEditors.XtraForm
    {
        public adduser()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this,"Add user?","Confirm",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) { 
            Useraccts usr = new Useraccts();
            if(txtpass.Text == txtcnfrmpass.Text)
            {
                usr.password = txtpass.Text;
                usr.user = txtuser.Text;
                usr.usrtype = cmbuser.Text;

                    try
                    {
                        db.Useraccts.Add(usr);
                        db.SaveChanges();
                        MessageBox.Show(this, "User Added sucessfully", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Dispose();
                    }
                    catch
                    {
                        MessageBox.Show(this,"Invalid Entry","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    }
            }
            else
            {
                    MessageBox.Show(this,"Password don't matched","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            }
        }

        private void labelControl5_Click(object sender, EventArgs e)
        {

        }
    }
}
