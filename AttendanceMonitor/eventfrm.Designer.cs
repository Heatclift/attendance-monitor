﻿namespace AttendanceMonitor
{
    partial class eventfrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgveventlist = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datetimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.athletesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attendancelistDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cntx1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rEmoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.numdays = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dtgveventlist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).BeginInit();
            this.cntx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numdays)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgveventlist
            // 
            this.dtgveventlist.AllowUserToAddRows = false;
            this.dtgveventlist.AllowUserToDeleteRows = false;
            this.dtgveventlist.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dtgveventlist.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgveventlist.AutoGenerateColumns = false;
            this.dtgveventlist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgveventlist.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgveventlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dtgveventlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgveventlist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.eventDataGridViewTextBoxColumn,
            this.datetimeDataGridViewTextBoxColumn,
            this.athletesDataGridViewTextBoxColumn,
            this.attendancelistDataGridViewTextBoxColumn});
            this.dtgveventlist.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtgveventlist.DataSource = this.eventsBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgveventlist.DefaultCellStyle = dataGridViewCellStyle3;
            this.dtgveventlist.Location = new System.Drawing.Point(3, 80);
            this.dtgveventlist.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtgveventlist.Name = "dtgveventlist";
            this.dtgveventlist.ReadOnly = true;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dtgveventlist.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgveventlist.RowTemplate.Height = 24;
            this.dtgveventlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgveventlist.Size = new System.Drawing.Size(707, 280);
            this.dtgveventlist.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // eventDataGridViewTextBoxColumn
            // 
            this.eventDataGridViewTextBoxColumn.DataPropertyName = "event";
            this.eventDataGridViewTextBoxColumn.HeaderText = "Event";
            this.eventDataGridViewTextBoxColumn.Name = "eventDataGridViewTextBoxColumn";
            this.eventDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // datetimeDataGridViewTextBoxColumn
            // 
            this.datetimeDataGridViewTextBoxColumn.DataPropertyName = "date_time";
            this.datetimeDataGridViewTextBoxColumn.HeaderText = "Date";
            this.datetimeDataGridViewTextBoxColumn.Name = "datetimeDataGridViewTextBoxColumn";
            this.datetimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // athletesDataGridViewTextBoxColumn
            // 
            this.athletesDataGridViewTextBoxColumn.DataPropertyName = "Athletes";
            this.athletesDataGridViewTextBoxColumn.HeaderText = "Athletes";
            this.athletesDataGridViewTextBoxColumn.Name = "athletesDataGridViewTextBoxColumn";
            this.athletesDataGridViewTextBoxColumn.ReadOnly = true;
            this.athletesDataGridViewTextBoxColumn.Visible = false;
            // 
            // attendancelistDataGridViewTextBoxColumn
            // 
            this.attendancelistDataGridViewTextBoxColumn.DataPropertyName = "Attendancelist";
            this.attendancelistDataGridViewTextBoxColumn.HeaderText = "Attendancelist";
            this.attendancelistDataGridViewTextBoxColumn.Name = "attendancelistDataGridViewTextBoxColumn";
            this.attendancelistDataGridViewTextBoxColumn.ReadOnly = true;
            this.attendancelistDataGridViewTextBoxColumn.Visible = false;
            // 
            // eventsBindingSource
            // 
            this.eventsBindingSource.DataSource = typeof(AttendanceMonitor.Events);
            // 
            // cntx1
            // 
            this.cntx1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cntx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rEmoveToolStripMenuItem});
            this.cntx1.Name = "cntx1";
            this.cntx1.Size = new System.Drawing.Size(118, 26);
            // 
            // rEmoveToolStripMenuItem
            // 
            this.rEmoveToolStripMenuItem.Name = "rEmoveToolStripMenuItem";
            this.rEmoveToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.rEmoveToolStripMenuItem.Text = "Remove";
            this.rEmoveToolStripMenuItem.Click += new System.EventHandler(this.rEmoveToolStripMenuItem_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(604, 22);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(96, 45);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Add Event|Day";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(34, 35);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Number of days:";
            // 
            // numdays
            // 
            this.numdays.Location = new System.Drawing.Point(144, 35);
            this.numdays.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numdays.Name = "numdays";
            this.numdays.Size = new System.Drawing.Size(39, 21);
            this.numdays.TabIndex = 3;
            this.numdays.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numdays.ValueChanged += new System.EventHandler(this.numdays_ValueChanged);
            // 
            // eventfrm
            // 
            this.ActiveGlowColor = System.Drawing.Color.Red;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 362);
            this.Controls.Add(this.numdays);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.dtgveventlist);
            this.HtmlText = "Events|Days";
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "eventfrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Activated += new System.EventHandler(this.eventfrm_Activated);
            this.Load += new System.EventHandler(this.eventfrm_Load);
            this.Enter += new System.EventHandler(this.eventfrm_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dtgveventlist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).EndInit();
            this.cntx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numdays)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgveventlist;
        private System.Windows.Forms.BindingSource eventsBindingSource;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datetimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn athletesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn attendancelistDataGridViewTextBoxColumn;
        private System.Windows.Forms.ContextMenuStrip cntx1;
        private System.Windows.Forms.ToolStripMenuItem rEmoveToolStripMenuItem;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.NumericUpDown numdays;
    }
}