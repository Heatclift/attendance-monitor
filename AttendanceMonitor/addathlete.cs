﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class addathlete : DevExpress.XtraEditors.XtraForm
    {
        AttendanceEntities1 db = new AttendanceEntities1();
        public addathlete()
        {
            InitializeComponent();
        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Athletes lis = new Athletes();
            lis.@event = cmbday.Text;
            lis.studno = txtid.Text;
            
            
                if (txtid.Text != "")
                {
                    if (MessageBox.Show(this, "Add Aprticipant?", "Attend Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                       
                            try
                            {
                                db.Athletes.Add(lis);
                                db.SaveChanges();
                                MessageBox.Show(this, "Participant added!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Dispose();
                            }
                            catch
                            {
                                MessageBox.Show(this, "404 Student not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                      
                    }
                }
                else
                {
                    MessageBox.Show(this, "Student no is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            
           
        }

        private void addathlete_Load(object sender, EventArgs e)
        {
            string[] lst = db.Student_Class_List.Select(x => x.Student_No).ToArray();
            var autosrc = new AutoCompleteStringCollection();
            autosrc.AddRange(lst);
            int noofdays = (int)db.Days.Where(x => x.day == "days").FirstOrDefault().dayno;
            int dayno = 1;
            txtid.AutoCompleteCustomSource = autosrc;
            string[] listofdays = new string[noofdays]; 
            for (int i =0;i< noofdays; i++) 
            {
                listofdays[i] = "Day " + dayno;
                dayno++;
            }
            cmbday.Items.AddRange(listofdays);
            
        }

        private void txtid_TextChanged(object sender, EventArgs e)
        {
            txtfname.Text = db.Student_Class_List.Where(x => x.Student_No == txtid.Text).Select(y => y.First_Name + " " + y.Last_Name).FirstOrDefault();
            txtcourse.Text = db.Student_Class_List.Where(x => x.Student_No == txtid.Text).Select(y => y.Program).FirstOrDefault();
        }
    }
}
