﻿namespace AttendanceMonitor
{
    partial class AddStud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtstudid = new DevExpress.XtraEditors.TextEdit();
            this.txtfname = new DevExpress.XtraEditors.TextEdit();
            this.txtlname = new DevExpress.XtraEditors.TextEdit();
            this.txtcourse = new DevExpress.XtraEditors.TextEdit();
            this.txtsection = new DevExpress.XtraEditors.TextEdit();
            this.btnadd = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtstudid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsection.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(18, 93);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(93, 21);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Student ID:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(20, 148);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 21);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "First Name:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(45, 313);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 21);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Section:";
            this.labelControl3.Click += new System.EventHandler(this.labelControl3_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(16, 203);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(95, 21);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Last Name:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(48, 258);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(63, 21);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Course:";
            this.labelControl5.Click += new System.EventHandler(this.labelControl5_Click);
            // 
            // txtstudid
            // 
            this.txtstudid.Location = new System.Drawing.Point(129, 95);
            this.txtstudid.Name = "txtstudid";
            this.txtstudid.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstudid.Properties.Appearance.Options.UseFont = true;
            this.txtstudid.Size = new System.Drawing.Size(293, 28);
            this.txtstudid.TabIndex = 5;
            // 
            // txtfname
            // 
            this.txtfname.Location = new System.Drawing.Point(129, 149);
            this.txtfname.Name = "txtfname";
            this.txtfname.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfname.Properties.Appearance.Options.UseFont = true;
            this.txtfname.Size = new System.Drawing.Size(293, 28);
            this.txtfname.TabIndex = 6;
            // 
            // txtlname
            // 
            this.txtlname.Location = new System.Drawing.Point(129, 203);
            this.txtlname.Name = "txtlname";
            this.txtlname.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlname.Properties.Appearance.Options.UseFont = true;
            this.txtlname.Size = new System.Drawing.Size(293, 28);
            this.txtlname.TabIndex = 7;
            // 
            // txtcourse
            // 
            this.txtcourse.Location = new System.Drawing.Point(129, 257);
            this.txtcourse.Name = "txtcourse";
            this.txtcourse.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcourse.Properties.Appearance.Options.UseFont = true;
            this.txtcourse.Size = new System.Drawing.Size(110, 28);
            this.txtcourse.TabIndex = 8;
            // 
            // txtsection
            // 
            this.txtsection.Location = new System.Drawing.Point(129, 311);
            this.txtsection.Name = "txtsection";
            this.txtsection.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsection.Properties.Appearance.Options.UseFont = true;
            this.txtsection.Size = new System.Drawing.Size(110, 28);
            this.txtsection.TabIndex = 9;
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(282, 269);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(140, 65);
            this.btnadd.TabIndex = 10;
            this.btnadd.Text = "Add";
            this.btnadd.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(143, 26);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(166, 33);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Add Student";
            // 
            // AddStud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 390);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.txtsection);
            this.Controls.Add(this.txtcourse);
            this.Controls.Add(this.txtlname);
            this.Controls.Add(this.txtfname);
            this.Controls.Add(this.txtstudid);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "AddStud";
            this.Text = "Add Student";
            ((System.ComponentModel.ISupportInitialize)(this.txtstudid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtlname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsection.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtstudid;
        private DevExpress.XtraEditors.TextEdit txtfname;
        private DevExpress.XtraEditors.TextEdit txtlname;
        private DevExpress.XtraEditors.TextEdit txtcourse;
        private DevExpress.XtraEditors.TextEdit txtsection;
        private DevExpress.XtraEditors.SimpleButton btnadd;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}