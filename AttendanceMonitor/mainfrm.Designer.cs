﻿namespace AttendanceMonitor
{
    partial class mainfrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainfrm));
            this.btnattm = new DevExpress.XtraEditors.SimpleButton();
            this.btnrep = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnevents = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.btnstud = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnattm
            // 
            this.btnattm.Appearance.Font = new System.Drawing.Font("Century Gothic", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnattm.Appearance.Options.UseFont = true;
            this.btnattm.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnattm.Location = new System.Drawing.Point(92, 125);
            this.btnattm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnattm.Name = "btnattm";
            this.btnattm.Size = new System.Drawing.Size(247, 115);
            this.btnattm.TabIndex = 0;
            this.btnattm.Text = "ATTENDANCE MONITOR";
            this.btnattm.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnrep
            // 
            this.btnrep.Appearance.Font = new System.Drawing.Font("Century Gothic", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnrep.Appearance.Options.UseFont = true;
            this.btnrep.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnrep.Location = new System.Drawing.Point(92, 263);
            this.btnrep.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnrep.Name = "btnrep";
            this.btnrep.Size = new System.Drawing.Size(247, 115);
            this.btnrep.TabIndex = 1;
            this.btnrep.Text = "REPORTS";
            this.btnrep.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(227, 28);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(436, 30);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "ATTENDANCE MONITORING SYSTEM CSACT";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // btnevents
            // 
            this.btnevents.Appearance.Font = new System.Drawing.Font("Century Gothic", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnevents.Appearance.Options.UseFont = true;
            this.btnevents.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnevents.Location = new System.Drawing.Point(371, 125);
            this.btnevents.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnevents.Name = "btnevents";
            this.btnevents.Size = new System.Drawing.Size(247, 115);
            this.btnevents.TabIndex = 4;
            this.btnevents.Text = "EVENTS";
            this.btnevents.Click += new System.EventHandler(this.btnevents_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Century Gothic", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.simpleButton4.Location = new System.Drawing.Point(371, 263);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(247, 115);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "PARTICIPANTS";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(26, 23);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(80, 41);
            this.simpleButton5.TabIndex = 7;
            this.simpleButton5.Text = "Users";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(111, 23);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(100, 41);
            this.simpleButton6.TabIndex = 8;
            this.simpleButton6.Text = "Config";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // btnstud
            // 
            this.btnstud.Appearance.Font = new System.Drawing.Font("Century Gothic", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnstud.Appearance.Options.UseFont = true;
            this.btnstud.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnstud.Location = new System.Drawing.Point(92, 399);
            this.btnstud.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnstud.Name = "btnstud";
            this.btnstud.Size = new System.Drawing.Size(247, 115);
            this.btnstud.TabIndex = 9;
            this.btnstud.Text = "STUDENTS";
            this.btnstud.Click += new System.EventHandler(this.btnstud_Click);
            // 
            // mainfrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(715, 537);
            this.Controls.Add(this.btnstud);
            this.Controls.Add(this.simpleButton6);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.btnevents);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnrep);
            this.Controls.Add(this.btnattm);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Glow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "mainfrm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendance Monitor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainfrm_FormClosed);
            this.Load += new System.EventHandler(this.mainfrm_Load);
            this.Click += new System.EventHandler(this.mainfrm_Click);
            this.Enter += new System.EventHandler(this.mainfrm_Enter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnattm;
        private DevExpress.XtraEditors.SimpleButton btnrep;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnevents;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton btnstud;
    }
}

