﻿namespace AttendanceMonitor
{
    partial class Attendancefrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Attendancefrm));
            this.lblhour = new DevExpress.XtraEditors.LabelControl();
            this.txtid = new System.Windows.Forms.TextBox();
            this.txtname = new DevExpress.XtraEditors.TextEdit();
            this.txtcourse = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnattend = new DevExpress.XtraEditors.SimpleButton();
            this.cmbevent = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbstate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtprevname = new DevExpress.XtraEditors.TextEdit();
            this.cmbhalf = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbevent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbstate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprevname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbhalf.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblhour
            // 
            this.lblhour.Appearance.Font = new System.Drawing.Font("Century Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhour.Appearance.Options.UseFont = true;
            this.lblhour.Location = new System.Drawing.Point(26, 28);
            this.lblhour.Name = "lblhour";
            this.lblhour.Size = new System.Drawing.Size(681, 142);
            this.lblhour.TabIndex = 0;
            this.lblhour.Text = "00:00:00 PM";
            this.lblhour.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // txtid
            // 
            this.txtid.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.txtid.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtid.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtid.Location = new System.Drawing.Point(98, 266);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(188, 30);
            this.txtid.TabIndex = 1;
            this.txtid.TextChanged += new System.EventHandler(this.txtid_TextChanged);
            this.txtid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtid_KeyDown);
            // 
            // txtname
            // 
            this.txtname.Enabled = false;
            this.txtname.Location = new System.Drawing.Point(296, 266);
            this.txtname.Name = "txtname";
            this.txtname.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Properties.Appearance.Options.UseFont = true;
            this.txtname.Size = new System.Drawing.Size(387, 28);
            this.txtname.TabIndex = 2;
            // 
            // txtcourse
            // 
            this.txtcourse.Enabled = false;
            this.txtcourse.Location = new System.Drawing.Point(689, 266);
            this.txtcourse.Name = "txtcourse";
            this.txtcourse.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcourse.Properties.Appearance.Options.UseFont = true;
            this.txtcourse.Size = new System.Drawing.Size(159, 28);
            this.txtcourse.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(134, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Student no.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(418, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "Student Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(693, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 22);
            this.label3.TabIndex = 6;
            this.label3.Text = "Student Course";
            // 
            // btnattend
            // 
            this.btnattend.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnattend.Appearance.Options.UseFont = true;
            this.btnattend.Location = new System.Drawing.Point(391, 330);
            this.btnattend.Name = "btnattend";
            this.btnattend.Size = new System.Drawing.Size(170, 69);
            this.btnattend.TabIndex = 7;
            this.btnattend.Text = "Attendance IN";
            this.btnattend.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cmbevent
            // 
            this.cmbevent.EditValue = "Day 1";
            this.cmbevent.Location = new System.Drawing.Point(717, 73);
            this.cmbevent.Name = "cmbevent";
            this.cmbevent.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbevent.Properties.Appearance.Options.UseFont = true;
            this.cmbevent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbevent.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbevent.Size = new System.Drawing.Size(205, 28);
            this.cmbevent.TabIndex = 8;
            this.cmbevent.SelectedIndexChanged += new System.EventHandler(this.cmbevent_SelectedIndexChanged);
            // 
            // cmbstate
            // 
            this.cmbstate.EditValue = "IN";
            this.cmbstate.Location = new System.Drawing.Point(717, 111);
            this.cmbstate.Name = "cmbstate";
            this.cmbstate.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbstate.Properties.Appearance.Options.UseFont = true;
            this.cmbstate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbstate.Properties.Items.AddRange(new object[] {
            "IN",
            "OUT"});
            this.cmbstate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbstate.Size = new System.Drawing.Size(88, 28);
            this.cmbstate.TabIndex = 9;
            this.cmbstate.SelectedIndexChanged += new System.EventHandler(this.cmbstate_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(787, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 22);
            this.label4.TabIndex = 10;
            this.label4.Text = "Day";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtprevname
            // 
            this.txtprevname.Enabled = false;
            this.txtprevname.Location = new System.Drawing.Point(2, 481);
            this.txtprevname.Name = "txtprevname";
            this.txtprevname.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprevname.Properties.Appearance.Options.UseFont = true;
            this.txtprevname.Size = new System.Drawing.Size(591, 28);
            this.txtprevname.TabIndex = 11;
            // 
            // cmbhalf
            // 
            this.cmbhalf.EditValue = "AM";
            this.cmbhalf.Location = new System.Drawing.Point(717, 145);
            this.cmbhalf.Name = "cmbhalf";
            this.cmbhalf.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbhalf.Properties.Appearance.Options.UseFont = true;
            this.cmbhalf.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbhalf.Properties.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.cmbhalf.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbhalf.Size = new System.Drawing.Size(88, 28);
            this.cmbhalf.TabIndex = 12;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(878, 454);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(44, 46);
            this.simpleButton1.TabIndex = 13;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // Attendancefrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 512);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cmbhalf);
            this.Controls.Add(this.txtprevname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbstate);
            this.Controls.Add(this.cmbevent);
            this.Controls.Add(this.btnattend);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcourse);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.lblhour);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Attendancefrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Attendance";
            this.Load += new System.EventHandler(this.Attendancefrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcourse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbevent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbstate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtprevname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbhalf.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblhour;
        private System.Windows.Forms.TextBox txtid;
        private DevExpress.XtraEditors.TextEdit txtname;
        private DevExpress.XtraEditors.TextEdit txtcourse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton btnattend;
        private DevExpress.XtraEditors.ComboBoxEdit cmbevent;
        private DevExpress.XtraEditors.ComboBoxEdit cmbstate;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtprevname;
        private DevExpress.XtraEditors.ComboBoxEdit cmbhalf;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}