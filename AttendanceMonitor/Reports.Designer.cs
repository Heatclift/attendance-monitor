﻿namespace AttendanceMonitor
{
    partial class Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.DoughnutSeriesLabel doughnutSeriesLabel1 = new DevExpress.XtraCharts.DoughnutSeriesLabel();
            DevExpress.XtraCharts.DoughnutSeriesView doughnutSeriesView1 = new DevExpress.XtraCharts.DoughnutSeriesView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmbcs = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbevent = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.chart1 = new DevExpress.XtraCharts.ChartControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cmbrep = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cmbcs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbevent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnutSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnutSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrep.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(717, 79);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Course:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // cmbcs
            // 
            this.cmbcs.EditValue = "----------------";
            this.cmbcs.Location = new System.Drawing.Point(811, 76);
            this.cmbcs.Name = "cmbcs";
            this.cmbcs.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcs.Properties.Appearance.Options.UseFont = true;
            this.cmbcs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbcs.Size = new System.Drawing.Size(235, 30);
            this.cmbcs.TabIndex = 1;
            this.cmbcs.SelectedIndexChanged += new System.EventHandler(this.cmbcs_SelectedIndexChanged);
            // 
            // cmbevent
            // 
            this.cmbevent.EditValue = "-----------------";
            this.cmbevent.Location = new System.Drawing.Point(811, 135);
            this.cmbevent.Name = "cmbevent";
            this.cmbevent.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbevent.Properties.Appearance.Options.UseFont = true;
            this.cmbevent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbevent.Size = new System.Drawing.Size(235, 30);
            this.cmbevent.TabIndex = 3;
            this.cmbevent.SelectedIndexChanged += new System.EventHandler(this.cmbevent_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(745, 138);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 23);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Day:";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // chart1
            // 
            this.chart1.AnimationStartMode = DevExpress.XtraCharts.ChartAnimationMode.OnDataChanged;
            this.chart1.Legend.Name = "Default Legend";
            this.chart1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chart1.Location = new System.Drawing.Point(25, 20);
            this.chart1.Name = "chart1";
            doughnutSeriesLabel1.TextPattern = "{A}: {VP:0%}";
            series1.Label = doughnutSeriesLabel1;
            series1.Name = "Att";
            series1.View = doughnutSeriesView1;
            this.chart1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.chart1.Size = new System.Drawing.Size(664, 361);
            this.chart1.TabIndex = 4;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(811, 253);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(235, 54);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "Generate";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cmbrep
            // 
            this.cmbrep.EditValue = "Summary";
            this.cmbrep.Location = new System.Drawing.Point(811, 194);
            this.cmbrep.Name = "cmbrep";
            this.cmbrep.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbrep.Properties.Appearance.Options.UseFont = true;
            this.cmbrep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbrep.Properties.Items.AddRange(new object[] {
            "Summary",
            "Detailed"});
            this.cmbrep.Size = new System.Drawing.Size(235, 30);
            this.cmbrep.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(720, 197);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 23);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Report:";
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 402);
            this.Controls.Add(this.cmbrep);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.cmbevent);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.cmbcs);
            this.Controls.Add(this.labelControl1);
            this.Name = "Reports";
            this.Text = "Reports";
            this.Load += new System.EventHandler(this.Reports_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbcs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbevent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnutSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnutSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrep.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbcs;
        private DevExpress.XtraEditors.ComboBoxEdit cmbevent;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraCharts.ChartControl chart1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbrep;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}