﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class login : DevExpress.XtraEditors.XtraForm
    {
        public login()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void login_Load(object sender, EventArgs e)
        {

        }

        private void btnlog_Click(object sender, EventArgs e)
        {
            try
            {
                var acc = db.Useraccts.Where(x => x.user == txtusr.Text && x.password == txtpass.Text).FirstOrDefault();
                switch (acc.usrtype)
                {
                    case "Admin":
                        mainfrm ad = new mainfrm();
                        ad.Show();
                        this.Hide();
                        break;
                    case "User":
                        Attendancefrm usr = new Attendancefrm();
                        usr.Show();
                        this.Hide();
                        break;
                    default:
                        MessageBox.Show(this,"Invalid user","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        break;
                }
            }
            catch
            {
                MessageBox.Show(this, "Connection Lost", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void hyperlinkLabelControl1_Click(object sender, EventArgs e)
        {

            Config frm = new Config();
            frm.ShowDialog();

        }

        private void txtpass_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnlog.PerformClick();
            }
        }
    }
}
