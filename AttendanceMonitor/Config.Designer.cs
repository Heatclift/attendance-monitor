﻿namespace AttendanceMonitor
{
    partial class Config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btntestcon = new DevExpress.XtraEditors.SimpleButton();
            this.txtsvpass = new DevExpress.XtraEditors.TextEdit();
            this.txtsvuser = new DevExpress.XtraEditors.TextEdit();
            this.txtdb = new DevExpress.XtraEditors.TextEdit();
            this.txtsv = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.bntsave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsvpass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsvuser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsv.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.checkEdit1);
            this.groupControl2.Controls.Add(this.btntestcon);
            this.groupControl2.Controls.Add(this.txtsvpass);
            this.groupControl2.Controls.Add(this.txtsvuser);
            this.groupControl2.Controls.Add(this.txtdb);
            this.groupControl2.Controls.Add(this.txtsv);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Location = new System.Drawing.Point(62, 143);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(481, 267);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Connection";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(20, 29);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Custom Conection";
            this.checkEdit1.Size = new System.Drawing.Size(134, 20);
            this.checkEdit1.TabIndex = 18;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // btntestcon
            // 
            this.btntestcon.Enabled = false;
            this.btntestcon.Location = new System.Drawing.Point(206, 208);
            this.btntestcon.Name = "btntestcon";
            this.btntestcon.Size = new System.Drawing.Size(104, 40);
            this.btntestcon.TabIndex = 17;
            this.btntestcon.Text = "Test Connection";
            this.btntestcon.Click += new System.EventHandler(this.btntestcon_Click);
            // 
            // txtsvpass
            // 
            this.txtsvpass.Enabled = false;
            this.txtsvpass.Location = new System.Drawing.Point(149, 165);
            this.txtsvpass.Name = "txtsvpass";
            this.txtsvpass.Properties.UseSystemPasswordChar = true;
            this.txtsvpass.Size = new System.Drawing.Size(236, 22);
            this.txtsvpass.TabIndex = 16;
            // 
            // txtsvuser
            // 
            this.txtsvuser.Enabled = false;
            this.txtsvuser.Location = new System.Drawing.Point(149, 137);
            this.txtsvuser.Name = "txtsvuser";
            this.txtsvuser.Size = new System.Drawing.Size(236, 22);
            this.txtsvuser.TabIndex = 15;
            // 
            // txtdb
            // 
            this.txtdb.Enabled = false;
            this.txtdb.Location = new System.Drawing.Point(149, 109);
            this.txtdb.Name = "txtdb";
            this.txtdb.Size = new System.Drawing.Size(236, 22);
            this.txtdb.TabIndex = 14;
            // 
            // txtsv
            // 
            this.txtsv.Enabled = false;
            this.txtsv.Location = new System.Drawing.Point(149, 81);
            this.txtsv.Name = "txtsv";
            this.txtsv.Size = new System.Drawing.Size(236, 22);
            this.txtsv.TabIndex = 13;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(71, 168);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 16);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Password:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(100, 140);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 16);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "User:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(73, 112);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(58, 16);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Database:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(88, 84);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 16);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "Server:";
            // 
            // bntsave
            // 
            this.bntsave.Location = new System.Drawing.Point(500, 427);
            this.bntsave.Name = "bntsave";
            this.bntsave.Size = new System.Drawing.Size(97, 41);
            this.bntsave.TabIndex = 5;
            this.bntsave.Text = "SAVE";
            this.bntsave.Click += new System.EventHandler(this.bntsave_Click);
            // 
            // Config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 480);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.bntsave);
            this.Name = "Config";
            this.Text = "Config";
            this.Load += new System.EventHandler(this.Config_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsvpass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsvuser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtsv.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btntestcon;
        private DevExpress.XtraEditors.TextEdit txtsvpass;
        private DevExpress.XtraEditors.TextEdit txtsvuser;
        private DevExpress.XtraEditors.TextEdit txtdb;
        private DevExpress.XtraEditors.TextEdit txtsv;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton bntsave;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
    }
}