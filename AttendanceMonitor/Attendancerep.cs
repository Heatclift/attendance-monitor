﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;

namespace AttendanceMonitor
{
    public partial class Attendancerep : DevExpress.XtraReports.UI.XtraReport
    {
        public Attendancerep()
        {
            InitializeComponent();
        }

        public void InitData(List<tblsumtemp> data,bool partcon,string cours)
        {
            
            objectDataSource1.DataSource = data;
            Course.Value = cours;
            if (partcon)
            {
                partlab.Visible = true;
            }
            else
            {
                partlab.Visible = false;
            }
        }
    }
}
