﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttendanceMonitor
{
    public class tbldetailedrep
    {
        public string studentId { get; set; }
        public string name { get; set; }
        public string inam { get; set; }
        public string outam { get; set; }
        public string inpm { get; set; }
        public string outpm { get; set; }
        public string section { get; set; }
    }
}
