﻿namespace AttendanceMonitor
{
    partial class Accounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgtvacct = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usrtypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useracctsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cntx1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rEmoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgtvacct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useracctsBindingSource)).BeginInit();
            this.cntx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgtvacct
            // 
            this.dgtvacct.AllowUserToAddRows = false;
            this.dgtvacct.AllowUserToDeleteRows = false;
            this.dgtvacct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgtvacct.AutoGenerateColumns = false;
            this.dgtvacct.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgtvacct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgtvacct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.userDataGridViewTextBoxColumn,
            this.passwordDataGridViewTextBoxColumn,
            this.usrtypeDataGridViewTextBoxColumn});
            this.dgtvacct.ContextMenuStrip = this.cntx1;
            this.dgtvacct.DataSource = this.useracctsBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 7.8F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgtvacct.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgtvacct.Location = new System.Drawing.Point(12, 87);
            this.dgtvacct.Name = "dgtvacct";
            this.dgtvacct.ReadOnly = true;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgtvacct.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgtvacct.RowTemplate.Height = 24;
            this.dgtvacct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgtvacct.Size = new System.Drawing.Size(741, 421);
            this.dgtvacct.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // userDataGridViewTextBoxColumn
            // 
            this.userDataGridViewTextBoxColumn.DataPropertyName = "user";
            this.userDataGridViewTextBoxColumn.HeaderText = "User";
            this.userDataGridViewTextBoxColumn.Name = "userDataGridViewTextBoxColumn";
            this.userDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // passwordDataGridViewTextBoxColumn
            // 
            this.passwordDataGridViewTextBoxColumn.DataPropertyName = "password";
            this.passwordDataGridViewTextBoxColumn.HeaderText = "password";
            this.passwordDataGridViewTextBoxColumn.Name = "passwordDataGridViewTextBoxColumn";
            this.passwordDataGridViewTextBoxColumn.ReadOnly = true;
            this.passwordDataGridViewTextBoxColumn.Visible = false;
            // 
            // usrtypeDataGridViewTextBoxColumn
            // 
            this.usrtypeDataGridViewTextBoxColumn.DataPropertyName = "usrtype";
            this.usrtypeDataGridViewTextBoxColumn.HeaderText = "User type";
            this.usrtypeDataGridViewTextBoxColumn.Name = "usrtypeDataGridViewTextBoxColumn";
            this.usrtypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // useracctsBindingSource
            // 
            this.useracctsBindingSource.DataSource = typeof(AttendanceMonitor.Useraccts);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(634, 24);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(119, 48);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Add user";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cntx1
            // 
            this.cntx1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cntx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rEmoveToolStripMenuItem});
            this.cntx1.Name = "cntx1";
            this.cntx1.Size = new System.Drawing.Size(133, 28);
            this.cntx1.Opening += new System.ComponentModel.CancelEventHandler(this.cntx1_Opening);
            // 
            // rEmoveToolStripMenuItem
            // 
            this.rEmoveToolStripMenuItem.Name = "rEmoveToolStripMenuItem";
            this.rEmoveToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.rEmoveToolStripMenuItem.Text = "Remove";
            this.rEmoveToolStripMenuItem.Click += new System.EventHandler(this.rEmoveToolStripMenuItem_Click);
            // 
            // Accounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 520);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.dgtvacct);
            this.Name = "Accounts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Accounts";
            this.Activated += new System.EventHandler(this.Accounts_Activated);
            this.Load += new System.EventHandler(this.Accounts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgtvacct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useracctsBindingSource)).EndInit();
            this.cntx1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgtvacct;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn passwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usrtypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource useracctsBindingSource;
        private System.Windows.Forms.ContextMenuStrip cntx1;
        private System.Windows.Forms.ToolStripMenuItem rEmoveToolStripMenuItem;
    }
}