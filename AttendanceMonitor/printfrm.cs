﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class printfrm : DevExpress.XtraEditors.XtraForm
    {
        AttendanceEntities1 db = new AttendanceEntities1();
        public printfrm(bool ispart, string eve, string course,string type)
        {
            InitializeComponent();
            switch (type)
            {
                case "Summary":
                    sumreport(ispart, eve, course);
                    break;
                case "Detailed":
                    detailedreport(eve, course);
                    break;
            }

           
        }

        private void printfrm_Load(object sender, EventArgs e)
        {

        }

        public void sumreport(bool ispart, string eve, string course)
        {
            List<tblsumtemp> attlist = new List<tblsumtemp>();
            AttendanceEntities1 rdb = new AttendanceEntities1();
            foreach (var i in db.Student_Class_List.Where(x => x.Program == course))
            {
                tblsumtemp model = new tblsumtemp();
                model.name = i.First_Name + " " + i.Last_Name;
                model.studentId = i.Student_No;
                model.no_absent = (int)rdb.Days.Where(x => x.day == "days").FirstOrDefault().dayno * 2 - rdb.Attendancelist.Where(x => x.fk_studid == i.Student_No && x.state == "OUT").Count();
                model.no_present = rdb.Attendancelist.Where(x => x.fk_studid == i.Student_No && x.state == "OUT").Count();
                attlist.Add(model);
            }
            Attendancerep rep = new Attendancerep();
            foreach (DevExpress.XtraReports.Parameters.Parameter p in rep.Parameters)
            {
                p.Visible = false;
            }
            rep.InitData(attlist.OrderBy(x => x.name).ToList<tblsumtemp>(), ispart, course);
            dv1.DocumentSource = rep;
            rep.CreateDocument();
        }
        public void detailedreport(string eve, string course)
        {
            int day = 0;
            AttendanceEntities1 rdb = new AttendanceEntities1();
            day = Convert.ToInt32(eve.Replace("Day ", string.Empty));
                
                List<tbldetailedrep> detailrep = new List<tbldetailedrep>();
                 //var ids = from x in db.Attendancelist
                 //   from y in db.Student_Class_List
                 //   where y.Program == course && y.Student_No == x.fk_studid
                 //   select new
                 //   {
                 //       id = y.Student_No,
                 //       name = y.First_Name + " " + y.Last_Name,
                 //       studentId = y.Student_No,
                 //       inam = x.type == "IN" && x.type == "AM" && x.date_time != null ? x.date_time. : ""
                 //   }; 

                foreach (var i in db.Student_Class_List.Where(x => x.Program == course))
                {
                string id = i.Student_No;
                    tbldetailedrep model = new tbldetailedrep();
                    model.name = i.First_Name + " " + i.Last_Name;
                    model.studentId = i.Student_No;
                try
                {
                    //  var a = rdb.Attendancelist.Where(x => x.day == day && x.fk_studid == id && x.state == "IN" && x.type == "AM").FirstOrDefault();
                    //model.inam = a != null ? a.date_time.ToString() : "N/A";
                    model.inam = rdb.Attendancelist.Where(x => x.day == day && x.fk_studid == id && x.state == "IN" && x.type == "AM").FirstOrDefault().date_time.Value.ToString("hh:mm tt");
                }
                catch {
                    model.inam = "N/A";
                }

                try
                {
                    model.outpm = rdb.Attendancelist.Where(x => x.day == day && x.fk_studid == id && x.state == "OUT" && x.type == "PM").FirstOrDefault().date_time.Value.ToString("hh:mm tt");
                    }
                    catch {
                    model.outpm = "N/A";
                }
                    try
                    {
                        model.inpm = rdb.Attendancelist.Where(x => x.day == day && x.fk_studid == id && x.state == "IN" && x.type == "PM").FirstOrDefault().date_time.Value.ToString("hh:mm tt");
                    }
                    catch {
                    model.inpm = "N/A";
                }
                    try
                        {
                            model.outam = rdb.Attendancelist.Where(x => x.day == day && x.fk_studid == id && x.state == "OUT" && x.type == "AM").FirstOrDefault().date_time.Value.ToString("hh:mm tt");

                    }
                    catch {
                    model.outam = "N/A";
                }
                    model.section = i.Section;
                    detailrep.Add(model);
                }
                detailedrep rep = new detailedrep();
                foreach (DevExpress.XtraReports.Parameters.Parameter p in rep.Parameters)
                {
                    p.Visible = false;
                }
                rep.InitData(detailrep.OrderBy(x => x.name).ToList<tbldetailedrep>(), course, eve);
                dv1.DocumentSource = rep;
                rep.CreateDocument();
           
          
        }
    }
}
