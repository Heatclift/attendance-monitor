﻿namespace AttendanceMonitor
{
    partial class Students
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgstud = new System.Windows.Forms.DataGridView();
            this.btnaddstud = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblstudno = new DevExpress.XtraEditors.LabelControl();
            this.studentNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.omegaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.campusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.academicCareerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.programDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.courseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sectionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.athletesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attendancelistDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentClassListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgstud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentClassListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgstud
            // 
            this.dgstud.AllowUserToAddRows = false;
            this.dgstud.AllowUserToDeleteRows = false;
            this.dgstud.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgstud.AutoGenerateColumns = false;
            this.dgstud.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgstud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgstud.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.studentNoDataGridViewTextBoxColumn,
            this.omegaIDDataGridViewTextBoxColumn,
            this.column2DataGridViewTextBoxColumn,
            this.column3DataGridViewTextBoxColumn,
            this.campusDataGridViewTextBoxColumn,
            this.column5DataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.column7DataGridViewTextBoxColumn,
            this.firstNameDataGridViewTextBoxColumn,
            this.academicCareerDataGridViewTextBoxColumn,
            this.programDataGridViewTextBoxColumn,
            this.courseDataGridViewTextBoxColumn,
            this.sectionDataGridViewTextBoxColumn,
            this.athletesDataGridViewTextBoxColumn,
            this.attendancelistDataGridViewTextBoxColumn});
            this.dgstud.DataSource = this.studentClassListBindingSource;
            this.dgstud.Location = new System.Drawing.Point(10, 82);
            this.dgstud.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgstud.Name = "dgstud";
            this.dgstud.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgstud.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgstud.RowTemplate.Height = 24;
            this.dgstud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgstud.Size = new System.Drawing.Size(703, 358);
            this.dgstud.TabIndex = 0;
            // 
            // btnaddstud
            // 
            this.btnaddstud.Location = new System.Drawing.Point(605, 20);
            this.btnaddstud.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnaddstud.Name = "btnaddstud";
            this.btnaddstud.Size = new System.Drawing.Size(108, 38);
            this.btnaddstud.TabIndex = 1;
            this.btnaddstud.Text = "Add student";
            this.btnaddstud.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 66);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Number of Students: ";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // lblstudno
            // 
            this.lblstudno.Location = new System.Drawing.Point(114, 66);
            this.lblstudno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblstudno.Name = "lblstudno";
            this.lblstudno.Size = new System.Drawing.Size(6, 13);
            this.lblstudno.TabIndex = 3;
            this.lblstudno.Text = "0";
            // 
            // studentNoDataGridViewTextBoxColumn
            // 
            this.studentNoDataGridViewTextBoxColumn.DataPropertyName = "Student_No";
            this.studentNoDataGridViewTextBoxColumn.HeaderText = "Student_No";
            this.studentNoDataGridViewTextBoxColumn.Name = "studentNoDataGridViewTextBoxColumn";
            this.studentNoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // omegaIDDataGridViewTextBoxColumn
            // 
            this.omegaIDDataGridViewTextBoxColumn.DataPropertyName = "Omega_ID";
            this.omegaIDDataGridViewTextBoxColumn.HeaderText = "Omega_ID";
            this.omegaIDDataGridViewTextBoxColumn.Name = "omegaIDDataGridViewTextBoxColumn";
            this.omegaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.omegaIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // column2DataGridViewTextBoxColumn
            // 
            this.column2DataGridViewTextBoxColumn.DataPropertyName = "Column_2";
            this.column2DataGridViewTextBoxColumn.HeaderText = "Column_2";
            this.column2DataGridViewTextBoxColumn.Name = "column2DataGridViewTextBoxColumn";
            this.column2DataGridViewTextBoxColumn.ReadOnly = true;
            this.column2DataGridViewTextBoxColumn.Visible = false;
            // 
            // column3DataGridViewTextBoxColumn
            // 
            this.column3DataGridViewTextBoxColumn.DataPropertyName = "Column_3";
            this.column3DataGridViewTextBoxColumn.HeaderText = "Column_3";
            this.column3DataGridViewTextBoxColumn.Name = "column3DataGridViewTextBoxColumn";
            this.column3DataGridViewTextBoxColumn.ReadOnly = true;
            this.column3DataGridViewTextBoxColumn.Visible = false;
            // 
            // campusDataGridViewTextBoxColumn
            // 
            this.campusDataGridViewTextBoxColumn.DataPropertyName = "Campus";
            this.campusDataGridViewTextBoxColumn.HeaderText = "Campus";
            this.campusDataGridViewTextBoxColumn.Name = "campusDataGridViewTextBoxColumn";
            this.campusDataGridViewTextBoxColumn.ReadOnly = true;
            this.campusDataGridViewTextBoxColumn.Visible = false;
            // 
            // column5DataGridViewTextBoxColumn
            // 
            this.column5DataGridViewTextBoxColumn.DataPropertyName = "Column_5";
            this.column5DataGridViewTextBoxColumn.HeaderText = "Column_5";
            this.column5DataGridViewTextBoxColumn.Name = "column5DataGridViewTextBoxColumn";
            this.column5DataGridViewTextBoxColumn.ReadOnly = true;
            this.column5DataGridViewTextBoxColumn.Visible = false;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "Last_Name";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "Last_Name";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // column7DataGridViewTextBoxColumn
            // 
            this.column7DataGridViewTextBoxColumn.DataPropertyName = "Column_7";
            this.column7DataGridViewTextBoxColumn.HeaderText = "Column_7";
            this.column7DataGridViewTextBoxColumn.Name = "column7DataGridViewTextBoxColumn";
            this.column7DataGridViewTextBoxColumn.ReadOnly = true;
            this.column7DataGridViewTextBoxColumn.Visible = false;
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "First_Name";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "First_Name";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // academicCareerDataGridViewTextBoxColumn
            // 
            this.academicCareerDataGridViewTextBoxColumn.DataPropertyName = "Academic_Career";
            this.academicCareerDataGridViewTextBoxColumn.HeaderText = "Academic_Career";
            this.academicCareerDataGridViewTextBoxColumn.Name = "academicCareerDataGridViewTextBoxColumn";
            this.academicCareerDataGridViewTextBoxColumn.ReadOnly = true;
            this.academicCareerDataGridViewTextBoxColumn.Visible = false;
            // 
            // programDataGridViewTextBoxColumn
            // 
            this.programDataGridViewTextBoxColumn.DataPropertyName = "Program";
            this.programDataGridViewTextBoxColumn.HeaderText = "Course";
            this.programDataGridViewTextBoxColumn.Name = "programDataGridViewTextBoxColumn";
            this.programDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // courseDataGridViewTextBoxColumn
            // 
            this.courseDataGridViewTextBoxColumn.DataPropertyName = "Course";
            this.courseDataGridViewTextBoxColumn.HeaderText = "Course";
            this.courseDataGridViewTextBoxColumn.Name = "courseDataGridViewTextBoxColumn";
            this.courseDataGridViewTextBoxColumn.ReadOnly = true;
            this.courseDataGridViewTextBoxColumn.Visible = false;
            // 
            // sectionDataGridViewTextBoxColumn
            // 
            this.sectionDataGridViewTextBoxColumn.DataPropertyName = "Section";
            this.sectionDataGridViewTextBoxColumn.HeaderText = "Section";
            this.sectionDataGridViewTextBoxColumn.Name = "sectionDataGridViewTextBoxColumn";
            this.sectionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // athletesDataGridViewTextBoxColumn
            // 
            this.athletesDataGridViewTextBoxColumn.DataPropertyName = "Athletes";
            this.athletesDataGridViewTextBoxColumn.HeaderText = "Athletes";
            this.athletesDataGridViewTextBoxColumn.Name = "athletesDataGridViewTextBoxColumn";
            this.athletesDataGridViewTextBoxColumn.ReadOnly = true;
            this.athletesDataGridViewTextBoxColumn.Visible = false;
            // 
            // attendancelistDataGridViewTextBoxColumn
            // 
            this.attendancelistDataGridViewTextBoxColumn.DataPropertyName = "Attendancelist";
            this.attendancelistDataGridViewTextBoxColumn.HeaderText = "Attendancelist";
            this.attendancelistDataGridViewTextBoxColumn.Name = "attendancelistDataGridViewTextBoxColumn";
            this.attendancelistDataGridViewTextBoxColumn.ReadOnly = true;
            this.attendancelistDataGridViewTextBoxColumn.Visible = false;
            // 
            // studentClassListBindingSource
            // 
            this.studentClassListBindingSource.DataSource = typeof(AttendanceMonitor.Student_Class_List);
            // 
            // Students
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 450);
            this.Controls.Add(this.lblstudno);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnaddstud);
            this.Controls.Add(this.dgstud);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Students";
            this.Text = "Students";
            this.Activated += new System.EventHandler(this.Students_Activated);
            this.Load += new System.EventHandler(this.Students_Load);
            this.Enter += new System.EventHandler(this.Students_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dgstud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentClassListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgstud;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn omegaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn column2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn column3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn campusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn column5DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn column7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn academicCareerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn programDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn courseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sectionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn athletesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn attendancelistDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource studentClassListBindingSource;
        private DevExpress.XtraEditors.SimpleButton btnaddstud;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblstudno;
    }
}