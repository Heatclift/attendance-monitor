﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Students : DevExpress.XtraEditors.XtraForm
    {
        public Students()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AddStud stud = new AddStud();
            stud.ShowDialog();

        }
        private void refresh()
        {

        }
        private void fill()
        {
            List<Student_Class_List> students = new List<Student_Class_List>();
            students = db.Student_Class_List.ToList();
            students.OrderBy(x => x.Last_Name);
            dgstud.DataSource = students;
            lblstudno.Text = db.Student_Class_List.Count().ToString("0,000");
        }

        private void Students_Load(object sender, EventArgs e)
        {
            fill();
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void Students_Enter(object sender, EventArgs e)
        {
            fill();
        }

        private void Students_Activated(object sender, EventArgs e)
        {
            fill();
        }
    }
}
