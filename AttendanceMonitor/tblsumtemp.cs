﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttendanceMonitor
{
    public class tblsumtemp
    {
        public string studentId { get; set; }
        public string name { get; set; }
        public int no_present { get; set; }
        public int no_absent { get; set; }
    }
}
