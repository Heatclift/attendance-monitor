﻿namespace AttendanceMonitor
{
    partial class Atheletes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgvparticipants = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fkeventDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentClassListDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cntx1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rEmoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.athletesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnaddath = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvparticipants)).BeginInit();
            this.cntx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.athletesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvparticipants
            // 
            this.dtgvparticipants.AllowUserToAddRows = false;
            this.dtgvparticipants.AllowUserToDeleteRows = false;
            this.dtgvparticipants.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgvparticipants.AutoGenerateColumns = false;
            this.dtgvparticipants.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvparticipants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvparticipants.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.studnoDataGridViewTextBoxColumn,
            this.fkeventDataGridViewTextBoxColumn,
            this.eventsDataGridViewTextBoxColumn,
            this.studentClassListDataGridViewTextBoxColumn,
            this.course,
            this.eventDataGridViewTextBoxColumn});
            this.dtgvparticipants.ContextMenuStrip = this.cntx1;
            this.dtgvparticipants.DataSource = this.athletesBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 7.8F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvparticipants.DefaultCellStyle = dataGridViewCellStyle1;
            this.dtgvparticipants.Location = new System.Drawing.Point(4, 89);
            this.dtgvparticipants.Name = "dtgvparticipants";
            this.dtgvparticipants.ReadOnly = true;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dtgvparticipants.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dtgvparticipants.RowTemplate.Height = 24;
            this.dtgvparticipants.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvparticipants.Size = new System.Drawing.Size(933, 488);
            this.dtgvparticipants.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // studnoDataGridViewTextBoxColumn
            // 
            this.studnoDataGridViewTextBoxColumn.DataPropertyName = "studno";
            this.studnoDataGridViewTextBoxColumn.HeaderText = "Student no.";
            this.studnoDataGridViewTextBoxColumn.Name = "studnoDataGridViewTextBoxColumn";
            this.studnoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fkeventDataGridViewTextBoxColumn
            // 
            this.fkeventDataGridViewTextBoxColumn.DataPropertyName = "fk_event";
            this.fkeventDataGridViewTextBoxColumn.HeaderText = "fk_event";
            this.fkeventDataGridViewTextBoxColumn.Name = "fkeventDataGridViewTextBoxColumn";
            this.fkeventDataGridViewTextBoxColumn.ReadOnly = true;
            this.fkeventDataGridViewTextBoxColumn.Visible = false;
            // 
            // eventsDataGridViewTextBoxColumn
            // 
            this.eventsDataGridViewTextBoxColumn.DataPropertyName = "Events";
            this.eventsDataGridViewTextBoxColumn.HeaderText = "Events";
            this.eventsDataGridViewTextBoxColumn.Name = "eventsDataGridViewTextBoxColumn";
            this.eventsDataGridViewTextBoxColumn.ReadOnly = true;
            this.eventsDataGridViewTextBoxColumn.Visible = false;
            // 
            // studentClassListDataGridViewTextBoxColumn
            // 
            this.studentClassListDataGridViewTextBoxColumn.DataPropertyName = "sudentname";
            this.studentClassListDataGridViewTextBoxColumn.HeaderText = "Student Name";
            this.studentClassListDataGridViewTextBoxColumn.Name = "studentClassListDataGridViewTextBoxColumn";
            this.studentClassListDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // course
            // 
            this.course.DataPropertyName = "curse";
            this.course.HeaderText = "Course";
            this.course.Name = "course";
            this.course.ReadOnly = true;
            // 
            // eventDataGridViewTextBoxColumn
            // 
            this.eventDataGridViewTextBoxColumn.DataPropertyName = "event";
            this.eventDataGridViewTextBoxColumn.HeaderText = "Event";
            this.eventDataGridViewTextBoxColumn.Name = "eventDataGridViewTextBoxColumn";
            this.eventDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cntx1
            // 
            this.cntx1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cntx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rEmoveToolStripMenuItem});
            this.cntx1.Name = "cntx1";
            this.cntx1.Size = new System.Drawing.Size(133, 28);
            // 
            // rEmoveToolStripMenuItem
            // 
            this.rEmoveToolStripMenuItem.Name = "rEmoveToolStripMenuItem";
            this.rEmoveToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.rEmoveToolStripMenuItem.Text = "Remove";
            this.rEmoveToolStripMenuItem.Click += new System.EventHandler(this.rEmoveToolStripMenuItem_Click);
            // 
            // athletesBindingSource
            // 
            this.athletesBindingSource.DataSource = typeof(AttendanceMonitor.Athletes);
            // 
            // btnaddath
            // 
            this.btnaddath.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddath.Appearance.Options.UseFont = true;
            this.btnaddath.Location = new System.Drawing.Point(759, 23);
            this.btnaddath.Name = "btnaddath";
            this.btnaddath.Size = new System.Drawing.Size(178, 45);
            this.btnaddath.TabIndex = 1;
            this.btnaddath.Text = "Add Participant";
            this.btnaddath.Click += new System.EventHandler(this.btnaddath_Click);
            // 
            // Atheletes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 584);
            this.Controls.Add(this.btnaddath);
            this.Controls.Add(this.dtgvparticipants);
            this.Name = "Atheletes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Participants";
            this.Activated += new System.EventHandler(this.Atheletes_Activated);
            this.Load += new System.EventHandler(this.Atheletes_Load);
            this.Enter += new System.EventHandler(this.Atheletes_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvparticipants)).EndInit();
            this.cntx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.athletesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvparticipants;
        private DevExpress.XtraEditors.SimpleButton btnaddath;
        private System.Windows.Forms.BindingSource athletesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn studnoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fkeventDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentClassListDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn course;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventDataGridViewTextBoxColumn;
        private System.Windows.Forms.ContextMenuStrip cntx1;
        private System.Windows.Forms.ToolStripMenuItem rEmoveToolStripMenuItem;
    }
}