﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class Config : DevExpress.XtraEditors.XtraForm
    {
        public Config()
        {
            InitializeComponent();
            checkEdit1.Checked = Convert.ToBoolean(Properties.Settings.Default["costomcon"]);
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked)
            {
                txtdb.Enabled = true;
                txtsv.Enabled = true;
                txtsvpass.Enabled = true;
                txtsvuser.Enabled = true;
                btntestcon.Enabled = true;
            }
            else
            {
                txtdb.Enabled = false;
                txtsv.Enabled = false;
                txtsvpass.Enabled = false;
                txtsvuser.Enabled = false;
                btntestcon.Enabled = false;
                txtdb.Text = "";
                txtsv.Text = "";
                txtsvpass.Text = "";
                txtsvuser.Text = "";
            }
        }

        private void bntsave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default["Server"] = txtsv.Text;
            Properties.Settings.Default["Database"] = txtdb.Text;
            Properties.Settings.Default["dbUser"] = txtsvuser.Text;
            Properties.Settings.Default["dbPassword"] = txtsvpass.Text;
            Properties.Settings.Default["costomcon"] = checkEdit1.Checked;
            if (MessageBox.Show("Are you sure you want to Save Current settings?", "Alert", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {

                Properties.Settings.Default.Save();
                this.Dispose();
            }

        }

        private void Config_Load(object sender, EventArgs e)
        {
            txtsv.Text = Properties.Settings.Default["Server"].ToString();
            txtdb.Text = Properties.Settings.Default["Database"].ToString();
            txtsvuser.Text = Properties.Settings.Default["dbUser"].ToString();
            txtsvpass.Text = Properties.Settings.Default["dbPassword"].ToString();
        }

        private void btntestcon_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection contest = new SqlConnection("Data Source=" + txtsv.Text + ";Initial Catalog=" + txtdb.Text + ";User ID=" + txtsvuser.Text + ";Password=" + txtsvpass.Text))
                {
                    contest.Open();
                    contest.Close();
                }
                MessageBox.Show(this, "Connection Initialized", "Connection Success");
            }
            catch
            {
                MessageBox.Show(this, "Connection failed", "Connection ERROR");
            }
        }
    }
}
