﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class mainfrm : DevExpress.XtraEditors.XtraForm
    {
        AttendanceEntities1 db = new AttendanceEntities1();
        public mainfrm()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Attendancefrm frm = new Attendancefrm();
            frm.ShowDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Reports frm = new Reports();
            frm.ShowDialog();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void btnevents_Click(object sender, EventArgs e)
        {
            eventfrm frm = new eventfrm();
            frm.ShowDialog();
        }

        private void mainfrm_Load(object sender, EventArgs e)
        {
            check();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Atheletes frm = new Atheletes();
            frm.ShowDialog();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            Accounts frm = new Accounts();
            frm.ShowDialog();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            Config frm = new Config();
            frm.ShowDialog();
        }

        private void mainfrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnstud_Click(object sender, EventArgs e)
        {
            Students stud = new Students();
            stud.ShowDialog();
        }

        private void mainfrm_Click(object sender, EventArgs e)
        {
            check();
        }

        private void check()
        {
            if (db.Days.Where(x => x.day == "days").Count() == 0)
            {
                Days day = new Days();
                day.day = "days";
                day.dayno = 5;
                db.Days.Add(day);
                db.SaveChanges();
            }

            if (db.Student_Class_List.ToList().Count() <= 0)
            {
                btnrep.Enabled = false;
                btnattm.Enabled = false;
            }
            else
            {
                btnrep.Enabled = true;
                btnattm.Enabled = true;
            }

        }

        private void mainfrm_Enter(object sender, EventArgs e)
        {
            check();
        }
    }
}
