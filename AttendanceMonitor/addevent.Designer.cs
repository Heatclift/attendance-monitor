﻿namespace AttendanceMonitor
{
    partial class addevent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtevel = new DevExpress.XtraEditors.TextEdit();
            this.dt = new DevExpress.XtraEditors.DateEdit();
            this.btnaddeve = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(64, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(104, 21);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Event|Day:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(71, 84);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(97, 21);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Date/Time:";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // txtevel
            // 
            this.txtevel.Location = new System.Drawing.Point(181, 39);
            this.txtevel.Name = "txtevel";
            this.txtevel.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtevel.Properties.Appearance.Options.UseFont = true;
            this.txtevel.Size = new System.Drawing.Size(234, 28);
            this.txtevel.TabIndex = 2;
            // 
            // dt
            // 
            this.dt.EditValue = null;
            this.dt.Location = new System.Drawing.Point(180, 81);
            this.dt.Name = "dt";
            this.dt.Properties.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt.Properties.Appearance.Options.UseFont = true;
            this.dt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dt.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dt.Size = new System.Drawing.Size(234, 28);
            this.dt.TabIndex = 3;
            // 
            // btnaddeve
            // 
            this.btnaddeve.Appearance.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddeve.Appearance.Options.UseFont = true;
            this.btnaddeve.Location = new System.Drawing.Point(181, 124);
            this.btnaddeve.Name = "btnaddeve";
            this.btnaddeve.Size = new System.Drawing.Size(233, 56);
            this.btnaddeve.TabIndex = 4;
            this.btnaddeve.Text = "Add Event";
            this.btnaddeve.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // addevent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 209);
            this.Controls.Add(this.btnaddeve);
            this.Controls.Add(this.dt);
            this.Controls.Add(this.txtevel);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "addevent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Event";
            ((System.ComponentModel.ISupportInitialize)(this.txtevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtevel;
        private DevExpress.XtraEditors.DateEdit dt;
        private DevExpress.XtraEditors.SimpleButton btnaddeve;
    }
}