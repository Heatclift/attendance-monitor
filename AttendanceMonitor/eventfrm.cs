﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceMonitor
{
    public partial class eventfrm : DevExpress.XtraEditors.XtraForm
    {
        public eventfrm()
        {
            InitializeComponent();
        }
        AttendanceEntities1 db = new AttendanceEntities1();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            addevent eve = new addevent();
            eve.ShowDialog();
        }

        private void eventfrm_Load(object sender, EventArgs e)
        {
            refresh();
           
        }
        private void refresh()
        {
            if(db.Days.Where(x => x.day == "days").Count() == 0)
            {
                Days day = new Days();
                day.day = "days";
                day.dayno = 5;
                db.Days.Add(day);
                db.SaveChanges();
            }
            List<Events> eve = db.Events.ToList();
            dtgveventlist.DataSource = HelpTools.ToDataTable(eve);
            numdays.Value = (decimal)db.Days.Where(x => x.day == "days").FirstOrDefault().dayno;
        }

        private void eventfrm_Enter(object sender, EventArgs e)
        {
            refresh();
        }

        private void eventfrm_Activated(object sender, EventArgs e)
        {
            refresh();
        }

        private void rEmoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow rows in dtgveventlist.SelectedRows)
            {
               
                int id = Convert.ToInt32(rows.Cells["iDDataGridViewTextBoxColumn"].Value);
                Events recrev = db.Events.Where(x => x.ID == id).FirstOrDefault();
                db.Events.Remove(recrev);
                
            }
            if (MessageBox.Show("Are you sure you want to REMOVE?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {

                db.SaveChanges();
            }
            refresh();
        }

        private void numdays_ValueChanged(object sender, EventArgs e)
        {
            db.Days.Where(x => x.day == "days").FirstOrDefault().dayno = (int)numdays.Value;
            db.SaveChanges();
        }
    }
}
